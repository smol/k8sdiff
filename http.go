package main

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"database/sql"
	_ "embed"
	"encoding/json"
	"fmt"
	"html"
	"html/template"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/handlers"
	"github.com/sergi/go-diff/diffmatchpatch"
)

const (
	cleanupInterval = 1 * time.Hour
	cleanupDays     = 15
)

var (
	//go:embed template.html
	tplStr string

	tpl = template.Must(template.New("").Funcs(map[string]any{
		"HTMLDiff": htmlDiff,
	}).Parse(tplStr))
)

func htmlDiff(diffs diffList) template.HTML {
	var buf bytes.Buffer
	for _, diff := range diffs {
		text := strings.Replace(html.EscapeString(diff.Text), "\n", "<br>", -1)
		switch diff.Type {
		case diffmatchpatch.DiffInsert:
			_, _ = buf.WriteString("<span class=\"diff-add\">")
			_, _ = buf.WriteString(text)
			_, _ = buf.WriteString("</span>")
		case diffmatchpatch.DiffDelete:
			_, _ = buf.WriteString("<span class=\"diff-remove\">")
			_, _ = buf.WriteString(text)
			_, _ = buf.WriteString("</span>")
		case diffmatchpatch.DiffEqual:
			_, _ = buf.WriteString("<span>")
			_, _ = buf.WriteString(text)
			_, _ = buf.WriteString("</span>")
		}
	}

	return template.HTML(buf.String()) //nolint:gosec
}

type diffService struct {
	db     *sql.DB
	userdb userdb
}

func newDiffService(db *sql.DB, userdb userdb) *diffService {
	s := &diffService{
		db:     db,
		userdb: userdb,
	}
	go s.cleanup()
	return s
}

type taggedDiffMap struct {
	L   []objectKey
	M   map[objectKey]diffList
	Tag string
}

func newTaggedDiffMap(om *orderedDiffMap, tag string) *taggedDiffMap {
	return &taggedDiffMap{
		L:   om.L,
		M:   om.M,
		Tag: tag,
	}
}

func (m *taggedDiffMap) Len() int { return len(m.L) }

func (s *diffService) handleGET(w http.ResponseWriter, req *http.Request) {
	id := strings.TrimPrefix(req.URL.Path, "/diff/")
	if id == "" {
		http.Error(w, "no id", http.StatusBadRequest)
		return
	}

	rec, ok := getDiff(s.db, id)
	if !ok {
		http.NotFound(w, req)
		return
	}

	added, deleted, changed := rec.Diff().partition(isDiffAdd, isDiffDel)

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Cache-Control", "max-age=604800")
	err := tpl.Execute(w, struct {
		Record     *diffRecord
		Added      *taggedDiffMap
		Deleted    *taggedDiffMap
		Changed    *taggedDiffMap
		NumChanges int
	}{
		Record:     rec,
		Added:      newTaggedDiffMap(added, "added"),
		Deleted:    newTaggedDiffMap(deleted, "deleted"),
		Changed:    newTaggedDiffMap(changed, "changed"),
		NumChanges: added.Len() + deleted.Len() + changed.Len(),
	})

	if err != nil {
		log.Printf("template error: %v", err)
	}
}

func (s *diffService) handlePOST(w http.ResponseWriter, req *http.Request) {
	oldData, err := yamlFromRequest(req, "old")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	newData, err := yamlFromRequest(req, "new")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	diff, numUnchanged := omDiff(oldData, newData)

	rec, err := newDiffRecord(diff, numUnchanged, req)
	if err != nil {
		log.Printf("error: newDiffRecord: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("received diff bundle: %d/%d objects, %d diffs (%d bytes)",
		oldData.Len(), newData.Len(), diff.Len(), len(rec.serializedDiff))

	if err := addToDB(s.db, rec); err != nil {
		log.Printf("error: saving record to database: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	//nolint:errcheck
	json.NewEncoder(w).Encode(struct {
		UUID string `json:"uuid"`
		URL  string `json:"url"`
	}{
		UUID: rec.ID,
		URL:  publicURL(req, fmt.Sprintf("/diff/%s", rec.ID)),
	})
}

func (s *diffService) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	switch {
	case req.URL.Path == "/" && req.Method == http.MethodPost:
		s.userdb.withAuth(w, req, s.handlePOST)
	case strings.HasPrefix(req.URL.Path, "/diff/") && req.Method == http.MethodGet:
		s.handleGET(w, req)
	default:
		http.NotFound(w, req)
	}
}

func (s *diffService) cleanup() {
	tick := time.NewTicker(cleanupInterval)
	for t := range tick.C {
		cutoff := t.AddDate(0, 0, -cleanupDays)
		if err := cleanupDB(s.db, cutoff); err != nil {
			log.Printf("database cleanup error: %v", err)
		}
	}
}

func publicURL(req *http.Request, path string) string {
	u := *req.URL
	u.Path = path

	// If the URL.Host field is not empty, it was probably already
	// set from X-Forwarded-Host. Otherwise try to pull the host
	// name from the Host header.
	if u.Host == "" {
		u.Host = req.Host
	}

	// Default scheme is http.
	if u.Scheme == "" {
		u.Scheme = "http"
	}

	return u.String()
}

func yamlFromRequest(req *http.Request, key string) (*orderedMap, error) {
	f, _, err := req.FormFile(key)
	if err != nil {
		return nil, fmt.Errorf("FormFile(): %w", err)
	}
	om, err := loadYAML(f)
	if err != nil {
		return nil, fmt.Errorf("invalid YAML: %w", err)
	}
	return om, nil
}

func remoteAddrFromRequest(req *http.Request) string {
	return stripPort(req.RemoteAddr)
}

func stripPort(s string) string {
	if host, _, err := net.SplitHostPort(s); err == nil {
		s = host
	}
	return s
}

// Let clients compress the request body via Content-Encoding: gzip.
//
// Note: 'curl' can't easily create such requests by itself,
// apparently you have to create the multipart/form-data body first,
// e.g. see for instance this snippet:
// https://gist.github.com/abbaspour/9680673dca3ce40c473d38510861cc7f
func decompressRequestMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Encoding") == "gzip" {
			gzr, err := gzip.NewReader(req.Body)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			defer gzr.Close()

			req.Body = gzr
		} else if req.Header.Get("Content-Encoding") == "zlib" {
			zr, err := zlib.NewReader(req.Body)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			defer zr.Close()

			req.Body = zr
		}

		h.ServeHTTP(w, req)
	})
}

func newServer(svc *diffService) *http.Server {
	return &http.Server{
		Handler: decompressRequestMiddleware(
			handlers.ProxyHeaders(
				handlers.CombinedLoggingHandler(os.Stdout, svc))),

		ReadTimeout:       2 * time.Minute,
		ReadHeaderTimeout: 10 * time.Second,
		WriteTimeout:      2 * time.Minute,
		IdleTimeout:       10 * time.Minute,
	}
}
