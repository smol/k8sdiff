package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
)

type diffRecord struct {
	ID             string
	serializedDiff []byte
	Timestamp      time.Time
	Description    string
	Link           string
	Auth           string
	RemoteAddr     string
	NumUnchanged   int
}

func newDiffRecord(diff *orderedDiffMap, numUnchanged int, req *http.Request) (*diffRecord, error) {
	id, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}

	return &diffRecord{
		ID:             id.String(),
		Timestamp:      time.Now(),
		Description:    req.FormValue("description"),
		Link:           req.FormValue("link"),
		Auth:           authFromRequest(req),
		RemoteAddr:     remoteAddrFromRequest(req),
		NumUnchanged:   numUnchanged,
		serializedDiff: marshalZstd(diff),
	}, nil
}

func (r *diffRecord) Diff() *orderedDiffMap {
	om := newOrderedDiffMap(128)
	unmarshalZstd(r.serializedDiff, om) //nolint:errcheck
	return om
}

var migrations = []string{
	`
CREATE TABLE diffs (
  id TEXT PRIMARY KEY NOT NULL,
  serialized_diff BLOB,
  num_unchanged INTEGER NOT NULL,
  timestamp DATETIME NOT NULL,
  description TEXT,
  link TEXT,
  auth TEXT,
  remote_addr TEXT
);
`,
}

func openDB(path string) (*sql.DB, error) {
	db, err := sql.Open(
		"sqlite3",
		path+"?_journal=WAL&_sync=OFF&_busy_timeout=999999",
	)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(1)

	if err := applyMigrations(db); err != nil {
		return nil, fmt.Errorf("failed to apply migrations: %w", err)
	}

	return db, nil
}

// Run 'f' within a transaction. The called function must call
// tx.Commit() if necessary.
func withTx(db *sql.DB, f func(tx *sql.Tx) error) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback() //nolint:errcheck

	return f(tx)
}

// Simple migrations support for a SQLite database.
func applyMigrations(db *sql.DB) error {
	return withTx(db, func(tx *sql.Tx) error {
		var idx int
		if err := tx.QueryRow("PRAGMA user_version").Scan(&idx); err != nil {
			return err
		}

		if idx == len(migrations) {
			return nil
		} else if idx > len(migrations) {
			return fmt.Errorf("database is at version %d which is more recent than this binary understands", idx)
		}

		for i, stmt := range migrations[idx:] {
			if _, err := tx.Exec(stmt); err != nil {
				return fmt.Errorf("migrating to version %d: %w", i, err)
			}
		}

		if n := len(migrations); n > 0 {
			if _, err := tx.Exec(fmt.Sprintf("PRAGMA user_version=%d", n)); err != nil {
				return err
			}
		}

		return tx.Commit()
	})
}

func addToDB(db *sql.DB, rec *diffRecord) error {
	return withTx(db, func(tx *sql.Tx) error {
		if _, err := tx.Exec(
			"INSERT INTO diffs (id, serialized_diff, num_unchanged, timestamp, description, link, auth, remote_addr) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
			rec.ID, rec.serializedDiff, rec.NumUnchanged, rec.Timestamp, rec.Description, rec.Link, rec.Auth, rec.RemoteAddr,
		); err != nil {
			return err
		}

		return tx.Commit()
	})
}

func getDiff(db *sql.DB, uuid string) (*diffRecord, bool) {
	var rec diffRecord

	if err := withTx(db, func(tx *sql.Tx) error {
		return tx.QueryRow(
			"SELECT id, serialized_diff, num_unchanged, timestamp, description, link, auth, remote_addr FROM diffs WHERE id = ?", uuid,
		).Scan(&rec.ID, &rec.serializedDiff, &rec.NumUnchanged, &rec.Timestamp, &rec.Description, &rec.Link, &rec.Auth, &rec.RemoteAddr)
	}); err != nil {
		return nil, false
	}

	return &rec, true
}

func cleanupDB(db *sql.DB, cutoff time.Time) error {
	return withTx(db, func(tx *sql.Tx) error {
		_, err := tx.Exec("DELETE FROM diffs WHERE timestamp < ?", cutoff)
		if err != nil {
			return err
		}

		return tx.Commit()
	})
}
