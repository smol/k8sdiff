package main

import (
	"time"

	"github.com/sergi/go-diff/diffmatchpatch"
)

// Set a timeout on diff calculations to mitigate malicious inputs.
// This is relatively short, as we expect to be processing lots of
// small texts.
const diffTimeout = 1 * time.Second

func isDiffEqual(diff diffList) bool {
	for _, el := range diff {
		if el.Type != diffmatchpatch.DiffEqual {
			return false
		}
	}
	return true
}

func isDiffAdd(diff diffList) bool {
	return len(diff) == 1 && diff[0].Type == diffmatchpatch.DiffInsert
}

func isDiffDel(diff diffList) bool {
	return len(diff) == 1 && diff[0].Type == diffmatchpatch.DiffDelete
}

func omDiffAddPair(om *orderedDiffMap, key objectKey, a, b string) bool {
	d := diffmatchpatch.New()
	d.DiffTimeout = diffTimeout

	// The following provides character-by-character diffs.
	//diff := d.DiffMain(a, b, true)

	// Instead, we use a different approach to generate
	// line-by-line diffs, which are easier to read.
	ra, rb, lines := d.DiffLinesToRunes(a, b)
	diff := d.DiffMainRunes(ra, rb, true)

	if !isDiffEqual(diff) {
		diff = d.DiffCleanupSemanticLossless(diff)
		diff = d.DiffCharsToLines(diff, lines)
		om.add(key, diff)
		return true
	}

	return false
}

// Compute per-object diffs of two orderedMaps.
func omDiff(a, b *orderedMap) (*orderedDiffMap, int) {
	diff := newOrderedDiffMap(64)

	var numUnchanged int
	tmp := make(map[objectKey]struct{}, len(a.L))
	for _, k := range a.L {
		tmp[k] = struct{}{}
	}
	for _, k := range b.L {
		if _, ok := tmp[k]; ok {
			if !omDiffAddPair(diff, k, a.M[k], b.M[k]) {
				numUnchanged++
			}
			delete(tmp, k)
		} else {
			omDiffAddPair(diff, k, "", b.M[k])
		}
	}
	// For ordered traversal of 'tmp' we still look at 'a.l'.
	for _, k := range a.L {
		if _, ok := tmp[k]; ok {
			omDiffAddPair(diff, k, a.M[k], "")
		}
	}

	return diff, numUnchanged
}
