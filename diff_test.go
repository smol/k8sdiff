package main

import (
	"testing"

	"github.com/sergi/go-diff/diffmatchpatch"
)

func loadTestdata(t *testing.T, oldPath, newPath string) (*orderedMap, *orderedMap) {
	t.Helper()

	oldObjs, err := loadYAMLFile(oldPath)
	if err != nil {
		t.Fatal(err)
	}

	newObjs, err := loadYAMLFile(newPath)
	if err != nil {
		t.Fatal(err)
	}

	return oldObjs, newObjs
}

func TestDiff_Large(t *testing.T) {
	oldObjs, newObjs := loadTestdata(t, "testdata/cluster.old.yaml", "testdata/cluster.new.yaml")

	diff, unchanged := omDiff(oldObjs, newObjs)

	if n := diff.Len(); n != 4 {
		t.Errorf("got %d diffs, expected 4", n)
	}
	if unchanged != 300 {
		t.Errorf("got %d unchanged objects, expected 300", unchanged)
	}
}

func TestDiff(t *testing.T) {
	oldObjs, newObjs := loadTestdata(t, "testdata/oneline.old.yaml", "testdata/oneline.new.yaml")

	diff, _ := omDiff(oldObjs, newObjs)

	if n := diff.Len(); n != 1 {
		t.Fatalf("got %d diffs, expected 1: %+v", n, diff.L)
	}

	dl := diff.M[diff.L[0]]

	// Now inspect the individual edits.
	expectedDiffs := []diffmatchpatch.Diff{
		{Type: diffmatchpatch.DiffEqual, Text: `apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  labels:
    tanka.dev/environment: 31645956a5f65e7085842cc6ef28bca5818cabc0ed662291
  name: idp
  namespace: auth
spec:
  endpoints:
  - port: idp-http-metrics
`},
		{Type: diffmatchpatch.DiffInsert, Text: `  - port: idp-audit-http-metrics
`},
		{Type: diffmatchpatch.DiffEqual, Text: `  namespaceSelector:
    matchNames:
    - auth
  selector:
    matchLabels:
      app: idp
`},
	}

	if len(dl) != len(expectedDiffs) {
		t.Fatalf("unexpected edit list: %+v", dl)
	}
	for i := 0; i < len(dl); i++ {
		if dl[i].Type != expectedDiffs[i].Type {
			t.Errorf("edit %d, bad type: %s (expected %s)", i, dl[i].Type, expectedDiffs[i].Type)
		}
		if dl[i].Text != expectedDiffs[i].Text {
			t.Errorf("edit %d, bad text: %s (expected %s)", i, dl[i].Text, expectedDiffs[i].Text)
		}
	}
}
