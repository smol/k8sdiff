package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
)

func makeTestService(t *testing.T) (*diffService, *httptest.Server, func()) {
	t.Helper()

	db, err := openDB(filepath.Join(t.TempDir(), "diff.db"))
	if err != nil {
		t.Fatal(err)
	}

	udb := []authUser{
		{Name: "testuser", Secret: "secret"},
	}
	svc := &diffService{db: db, userdb: udb}
	srv := httptest.NewServer(svc)

	return svc, srv, func() {
		srv.Close()
		db.Close()
	}
}

func makePostRequest(t *testing.T, uri, fileOld, fileNew, username, password string) *http.Request {
	t.Helper()

	var buf bytes.Buffer
	w := multipart.NewWriter(&buf)

	yf, _ := os.Open(fileOld)
	fw, err := w.CreateFormFile("old", "old.yml")
	if err != nil {
		t.Fatalf("CreateFormFile: %v", err)
	}
	io.Copy(fw, yf) //nolint:errcheck
	yf.Close()

	yf, _ = os.Open(fileNew)
	fw, err = w.CreateFormFile("new", "new.yml")
	if err != nil {
		t.Fatalf("CreateFormFile: %v", err)
	}
	io.Copy(fw, yf) //nolint:errcheck
	yf.Close()

	w.Close()

	req, err := http.NewRequest(http.MethodPost, uri, &buf)
	if err != nil {
		t.Fatalf("NewRequest: %v", err)
	}
	req.Header.Set("Content-Type", w.FormDataContentType())
	if username != "" {
		req.SetBasicAuth(username, password)
	}
	return req
}

func TestService_Ingest(t *testing.T) {
	_, srv, cleanup := makeTestService(t)
	defer cleanup()

	client := new(http.Client)

	for _, td := range []struct {
		oldFile, newFile string
	}{
		{"testdata/cluster.old.yaml", "testdata/cluster.new.yaml"},
		{"testdata/cluster.new.yaml", "testdata/cluster.old.yaml"},
		{"testdata/empty", "testdata/cluster.new.yaml"},
		{"testdata/cluster.old.yaml", "testdata/empty"},
		{"testdata/cluster.old.yaml", "testdata/cluster.old.yaml"},
		{"testdata/empty", "testdata/empty"},
	} {
		tag := fmt.Sprintf("%s -> %s", td.oldFile, td.newFile)

		req := makePostRequest(t, srv.URL, td.oldFile, td.newFile, "testuser", "secret")
		resp, err := client.Do(req)
		if err != nil {
			t.Errorf("%s: HTTP: %v", tag, err)
			continue
		}
		if resp.StatusCode != 200 {
			t.Errorf("%s: HTTP status %d", tag, resp.StatusCode)
			continue
		}

		var respStruct struct {
			UUID string `json:"uuid"`
		}
		err = json.NewDecoder(resp.Body).Decode(&respStruct)
		resp.Body.Close()
		if err != nil {
			t.Errorf("%s: json response error: %v", tag, err)
			continue
		}

		req, _ = http.NewRequest(http.MethodGet, srv.URL+"/diff/"+respStruct.UUID, nil)
		resp, err = client.Do(req)
		if err != nil {
			t.Fatalf("%s: HTTP: %v", tag, err)
			continue
		}
		io.ReadAll(resp.Body) //nolint:errcheck
		resp.Body.Close()
		if resp.StatusCode != 200 {
			t.Fatalf("%s: HTTP status %d", tag, resp.StatusCode)
			continue
		}
	}
}

func TestService_Auth(t *testing.T) {
	_, srv, cleanup := makeTestService(t)
	defer cleanup()

	client := new(http.Client)

	for _, td := range []struct {
		username       string
		password       string
		expectedStatus int
	}{
		{"", "", http.StatusUnauthorized},
		{"testuser", "bad secret", http.StatusForbidden},
		{"bad user", "secret", http.StatusForbidden},
		{"testuser", "secret", http.StatusOK},
	} {
		req := makePostRequest(t, srv.URL, "./testdata/cluster.old.yaml", "./testdata/cluster.new.yaml", td.username, td.password)
		resp, err := client.Do(req)
		if err != nil {
			t.Fatalf("HTTP: %v", err)
		}
		if resp.StatusCode != td.expectedStatus {
			t.Errorf("HTTP: u=%s,p=%s: status %d, expected %d", td.username, td.password, resp.StatusCode, td.expectedStatus)
		}
		resp.Body.Close()
	}
}
