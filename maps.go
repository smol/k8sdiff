package main

import (
	"errors"
	"fmt"
	"io"

	"github.com/sergi/go-diff/diffmatchpatch"
)

type objectKey struct {
	apiVersion string
	kind       string
	name       string
	namespace  string
}

func (k objectKey) String() string {
	return fmt.Sprintf("%s:%s:%s:%s", k.apiVersion, k.kind, k.namespace, k.name)
}

func (k objectKey) PrettyName() string {
	if k.namespace == "" {
		return k.name
	}
	return fmt.Sprintf("%s/%s", k.namespace, k.name)
}

func (k objectKey) PrettyKind() string {
	return k.kind
}

func (k objectKey) marshal(w io.Writer) error {
	if err := writeString(w, k.apiVersion); err != nil {
		return err
	}
	if err := writeString(w, k.kind); err != nil {
		return err
	}
	if err := writeString(w, k.name); err != nil {
		return err
	}
	return writeString(w, k.namespace)
}

func (k *objectKey) unmarshal(r io.Reader) (err error) {
	k.apiVersion, err = readString(r)
	if err != nil {
		return
	}
	k.kind, err = readString(r)
	if err != nil {
		return
	}
	k.name, err = readString(r)
	if err != nil {
		return
	}
	k.namespace, err = readString(r)
	return
}

type orderedMap struct {
	L []objectKey
	M map[objectKey]string
}

func newOrderedMap(pre int) *orderedMap {
	return &orderedMap{
		L: make([]objectKey, 0, pre),
		M: make(map[objectKey]string, pre),
	}
}

func (o *orderedMap) Len() int { return len(o.L) }

func (o *orderedMap) has(key objectKey) bool {
	_, ok := o.M[key]
	return ok
}

func (o *orderedMap) add(key objectKey, value string) {
	o.L = append(o.L, key)
	o.M[key] = value
}

func (o *orderedMap) marshal(w io.Writer) error {
	for _, k := range o.L {
		if err := k.marshal(w); err != nil {
			return err
		}
		if err := writeString(w, o.M[k]); err != nil {
			return err
		}
	}
	return nil
}

func (o *orderedMap) unmarshal(r io.Reader) error {
	for {
		var k objectKey
		err := k.unmarshal(r)
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return err
		}
		v, err := readString(r)
		if err != nil {
			return err
		}
		o.add(k, v)
	}
	return nil
}

type diffList []diffmatchpatch.Diff

func (l diffList) marshal(w io.Writer) error {
	if err := writeInt(w, len(l)); err != nil {
		return err
	}
	for i := 0; i < len(l); i++ {
		if err := writeInt(w, int(l[i].Type)); err != nil {
			return err
		}
		if err := writeString(w, l[i].Text); err != nil {
			return err
		}
	}
	return nil
}

func (l *diffList) unmarshal(r io.Reader) error {
	n, err := readInt(r)
	if err != nil {
		return err
	}

	ll := make([]diffmatchpatch.Diff, n)
	for i := 0; i < n; i++ {
		var d diffmatchpatch.Diff
		op, err := readInt(r)
		if err != nil {
			return err
		}
		d.Type = diffmatchpatch.Operation(op)
		d.Text, err = readString(r)
		if err != nil {
			return err
		}

		ll[i] = d
	}

	*l = ll

	return nil
}

type orderedDiffMap struct {
	L []objectKey
	M map[objectKey]diffList
}

func newOrderedDiffMap(pre int) *orderedDiffMap {
	return &orderedDiffMap{
		L: make([]objectKey, 0, pre),
		M: make(map[objectKey]diffList, pre),
	}
}

func (o *orderedDiffMap) Len() int { return len(o.L) }

func (o *orderedDiffMap) add(key objectKey, value diffList) {
	o.L = append(o.L, key)
	o.M[key] = value
}

func (o *orderedDiffMap) marshal(w io.Writer) error {
	for _, k := range o.L {
		if err := k.marshal(w); err != nil {
			return err
		}
		if err := o.M[k].marshal(w); err != nil {
			return err
		}
	}
	return nil
}

func (o *orderedDiffMap) unmarshal(r io.Reader) error {
	for {
		var k objectKey
		err := k.unmarshal(r)
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return err
		}
		var l diffList
		if err := l.unmarshal(r); err != nil {
			return err
		}
		o.add(k, l)
	}
	return nil
}

func (o *orderedDiffMap) partition(fnA, fnB func(diffList) bool) (*orderedDiffMap, *orderedDiffMap, *orderedDiffMap) {
	a := newOrderedDiffMap(o.Len())
	b := newOrderedDiffMap(o.Len())
	c := newOrderedDiffMap(o.Len())

	for _, k := range o.L {
		value := o.M[k]
		if fnA(value) {
			a.add(k, value)
		} else if fnB(value) {
			b.add(k, value)
		} else {
			c.add(k, value)
		}
	}

	return a, b, c
}
