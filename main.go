package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	addr       = flag.String("addr", ":4197", "address to listen on")
	dbPath     = flag.String("db", "diffs.db", "database path")
	userdbPath = flag.String("users", "users.yml", "user credentials path (YAML)")
)

func shutdownGracefully(srv *http.Server) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	srv.Shutdown(ctx) //nolint:errcheck
	cancel()
	srv.Close()
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	db, err := openDB(*dbPath)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	udb, err := loadUserDB(*userdbPath)
	if err != nil {
		log.Fatal(err)
	}
	if len(udb) > 0 {
		log.Printf("loaded %d authentication credentials", len(udb))
	}

	svc := newDiffService(db, udb)

	srv := newServer(svc)
	srv.Addr = *addr

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("signal received, terminating")
		shutdownGracefully(srv)
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	log.Printf("starting HTTP server on %s", *addr)
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
}
