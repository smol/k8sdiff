package main

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"

	"github.com/DataDog/zstd"
)

// Marshaler / unmarshaler for a very simplistic (but fast),
// string-oriented serialization format, which we use to work around
// container boxing/unboxing limitations and operate with minimal
// memory overhead. The results are then compressed with zstd.
type unmarshaler interface {
	unmarshal(io.Reader) error
}

type marshaler interface {
	marshal(io.Writer) error
}

// Integers are stored as little-endian int32s.
func readInt(r io.Reader) (int, error) {
	var i int32
	err := binary.Read(r, binary.LittleEndian, &i)
	return int(i), err
}

func writeInt(w io.Writer, i int) error {
	return binary.Write(w, binary.LittleEndian, int32(i))
}

// Strings are prefixed with a uint32 length.
func readString(r io.Reader) (string, error) {
	var sz uint32
	if err := binary.Read(r, binary.LittleEndian, &sz); err != nil {
		return "", err
	}
	buf := make([]byte, sz)
	n, err := io.ReadFull(r, buf)
	if err != nil {
		return "", err
	}
	if n != int(sz) {
		return "", errors.New("short read")
	}
	return string(buf), nil
}

func writeString(w io.Writer, s string) error {
	if err := binary.Write(w, binary.LittleEndian, uint32(len(s))); err != nil {
		return err
	}
	_, err := io.WriteString(w, s)
	return err
}

func marshalZstd(obj marshaler) []byte {
	var buf bytes.Buffer

	cw := zstd.NewWriter(&buf)
	defer cw.Close()

	if err := obj.marshal(cw); err != nil {
		panic(err)
	}
	cw.Flush()

	return buf.Bytes()
}

func unmarshalZstd(data []byte, out unmarshaler) error {
	cr := zstd.NewReader(bytes.NewReader(data))
	defer cr.Close()
	return out.unmarshal(cr)
}
