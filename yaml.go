package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"

	"gopkg.in/yaml.v3"
)

// Fast YAML scanner that reads objects as strings, but also supports
// minimal deserialization in order to extract primary key parameters.
type fastYAMLScanner struct {
	*bufio.Scanner
}

const maxScanTokenSize = 16 * 1024 * 1024

var yamlEOR = []byte{'\n', '-', '-', '-', '\n'}

func newFastYAMLScanner(r io.Reader) *fastYAMLScanner {
	b := make([]byte, maxScanTokenSize)

	s := bufio.NewScanner(r)
	s.Buffer(b, maxScanTokenSize)
	s.Split(func(data []byte, atEOF bool) (int, []byte, error) {
		if i := bytes.Index(data, yamlEOR); i >= 0 {
			return i + 5, data[:i+1], nil
		}
		if atEOF && len(data) > 0 {
			return len(data), data, nil
		}
		return 0, nil, nil
	})
	return &fastYAMLScanner{Scanner: s}
}

// Just enough of a Kubernetes object to build a primary key.
type yamlObject struct {
	ApiVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name      string `yaml:"name"`
		Namespace string `yaml:"namespace"`
	} `yaml:"metadata"`
}

func (o *yamlObject) Key() objectKey {
	return objectKey{
		apiVersion: o.ApiVersion,
		kind:       o.Kind,
		name:       o.Metadata.Name,
		namespace:  o.Metadata.Namespace,
	}
}

func parseObject(data []byte, out *yamlObject) error {
	return yaml.Unmarshal(data, out)
}

func loadYAML(r io.Reader) (*orderedMap, error) {
	o := newOrderedMap(128)

	scanner := newFastYAMLScanner(r)
	for scanner.Scan() {
		var obj yamlObject
		if err := parseObject(scanner.Bytes(), &obj); err != nil {
			return nil, fmt.Errorf("parseObject: %w", err)
		}

		key := obj.Key()
		if o.has(key) {
			return nil, fmt.Errorf("duplicate key: %s", key)
		}
		o.add(key, scanner.Text())
	}

	return o, scanner.Err()
}

func loadYAMLFile(path string) (*orderedMap, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return loadYAML(f)
}
