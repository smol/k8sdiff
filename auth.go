package main

import (
	"context"
	"net/http"
	"os"

	"gopkg.in/yaml.v3"
)

// Authentication is stored in a configuration file, not in the
// database (because it's not dynamic, and because it removes the
// problem of controlling write access to it).
//
// Since authentication is meant for server-to-server API access only,
// we use a trivial shared secret mechanism with credentials stored in
// cleartext.
type authUser struct {
	Name   string `yaml:"name"`
	Secret string `yaml:"secret"`
}

type userdb []authUser

func loadUserDB(path string) (userdb, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var udb userdb
	if err := yaml.NewDecoder(f).Decode(&udb); err != nil {
		return nil, err
	}
	return udb, nil
}

func (udb userdb) getUser(username string) (authUser, bool) {
	for _, user := range udb {
		if user.Name == username {
			return user, true
		}
	}
	return authUser{}, false
}

func (udb userdb) authenticate(username, secret string) bool {
	user, ok := udb.getUser(username)
	if !ok {
		return false
	}
	return user.Secret == secret
}

type authCtxType int

var authCtxKey authCtxType = 1

// Not a traditional http.Handler middleware, instead meant for direct
// invocation from a ServeHTTP method.
func (udb userdb) withAuth(w http.ResponseWriter, req *http.Request, h func(http.ResponseWriter, *http.Request)) {
	username, secret, ok := req.BasicAuth()
	if !ok {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}

	if !udb.authenticate(username, secret) {
		http.Error(w, "", http.StatusForbidden)
		return
	}

	req = req.WithContext(context.WithValue(req.Context(), authCtxKey, username))

	h(w, req)
}

func authFromRequest(req *http.Request) string {
	if s, ok := req.Context().Value(authCtxKey).(string); ok {
		return s
	}
	return ""
}
