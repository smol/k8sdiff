module git.autistici.org/smol/k8sdiff

go 1.21.8

require (
	github.com/DataDog/zstd v1.5.5
	github.com/google/uuid v1.6.0
	github.com/gorilla/handlers v1.5.2
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/sergi/go-diff v1.1.0
	gopkg.in/yaml.v3 v3.0.1
)

require github.com/felixge/httpsnoop v1.0.3 // indirect
