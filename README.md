k8sdiff
===

A trivial web service to render meaningful diffs between (large) YAML
files. Might be useful in a Kubernetes context, in combination with CI.

It operates as a standalone web service, with ingestion controlled by
simple authentication, and public viewing of the diffs (which have
unique, not easily guessable identifiers).

## Ingestion API

To compute a new diff, send a *multipart/form-data* POST request with
*old* and *new* file fields containing the YAML contents to compare.

For example, using *curl* and some made-up credentials:

```shell
curl -F old=a.yaml -F new=b.yaml --user myuser:mysecret \
    http://diffs.example.com/
```

Since YAML input files can be pretty large, k8sdiff supports
compressed requests (gzip/zlib), although *curl* can't generate those.

The response is a JSON object containing the public URL for the
rendered diff output.

### Gitlab CI integration

It's possible to integrate k8sdiff with Gitlab CI and have diff links
automatically added to the MR overview page, by leveraging [dotenv
artifacts](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv)
and [Gitlab
environments](https://docs.gitlab.com/ee/ci/environments/).

An example *.gitlab-ci.yml* snippet:

```yaml
diff:
  image: ...
  script:
    - # Generate your old/new YAML files somehow
    - # (possibly using $CI_MERGE_REQUEST_DIFF_BASE_SHA)
    -|
        curl -sf
        -o _curl.out
        -F old=@old.yml -F new=@new.yml
        -F description=$CI_COMMIT_REF_SLUG
        -F link=$CI_MERGE_REQUEST_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_IID
        --user $K8SDIFF_AUTH
        $K8SDIFF_URL
    - jq -r .url < _curl.out | sed -e s,^,DIFF_URL=, > diff.env
  artifacts:
    reports:
      dotenv: diff.env
  environment:
    name: diff-$CI_COMMIT_REF_SLUG
    url: $DIFF_URL
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
```

The above example requires setting the *K8SDIFF_URL* (location of the
k8sdiff service) and *K8SDIFF_AUTH* (credentials in the account:secret
format) CI variables.

## Storage

K8sdiff uses a SQLite database as its storage backend. Diffs expire
after 15 days, so the database doesn't grow forever.

## Authentication

The ingestion API requires authentication. As this is a
service-to-service context, authentication in k8sdiff is a very simple
affair involving shared secrets and HTTP Basic Auth, using credentials
stored in cleartext in a server configuration file.

The configuration file format is a YAML list of service accounts, each
with *name* and *secret* attributes, e.g.:

```yaml
- name: account1
  secret: secret1
- name: account2
  secret: secret2
```

Pass its location to the *k8sdiff* daemon using the *--user*
command-line option.

## Running the service

The service consists of the self-contained *k8sdiff* binary, which
offers a HTTP endpoint on port 4197 by default (can be changed with
the *--addr* option).

The [docker-compose.yml](docker-compose.yml) file in this repository
can be used to run the service (trivially) under docker-compose.
