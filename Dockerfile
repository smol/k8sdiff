FROM golang:1.21 AS build
ADD . /src
WORKDIR /src
RUN go build -ldflags=-extldflags=-static -tags "netgo sqlite_omit_load_extension" -trimpath -o /k8sdiff .

FROM scratch
COPY --from=build /k8sdiff /k8sdiff

ENTRYPOINT ["/k8sdiff"]
